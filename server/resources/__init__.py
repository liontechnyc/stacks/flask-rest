#!/usr/bin/env python3
from flask_sqlalchemy import SQLAlchemy
from flask_restful import Api

''' Application resources '''
db = SQLAlchemy()
api = Api()

''' Api endpoints '''
# from server.api.auth import Authentication
# from server.api.user import User
# from server.api.validation import ConfirmationValidation, RecoveryValidation