#!/usr/bin/env python3
from server.resources import api, db, errors
from flask_restful import Resource, reqparse

@api.resource('/endpoint', endpoint='example_endpoint')
class Endpoint(Resource):
    ''' Initialize endpoint argument parsers '''
    def __init__(self):
        self.parser = {
            'get' : reqparse.RequestParser(bundle_errors=True),
            'post' : reqparse.RequestParser(bundle_errors=True)
        }
        self.init_parser()

    def init_parser(self):
        # POST parser arguments
        self.parser['get'].add_argument('arg_1', trim=True, required=True, help='Email is required')
        # PUT parser arguments
        self.parser['post'].add_argument('arg_2', trim=True, help='User\'s name')

    '''  Read a resource '''
    def get(self):
        pass

    ''' Upload a new resource '''
    def post(self):
        pass
    