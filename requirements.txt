# Application
# boto3
# Development
Flask
SQLAlchemy
Flask-SQLAlchemy
Flask-Migrate
# Testing
pytest
pytest-env
# utils
bcrypt
python-dotenv
validate_email
flask-emails
flask-restful
flask-security
flask-cors
# Deployment
gunicorn
psycopg2-binary